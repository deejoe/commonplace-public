
OK, so my attentional path was thus:

Reading about cryptography more generally through a link someone posted.
I stay on the surface pretty much due to a vicious circle: There's a lot there I can't follow because I don't know enough.

https://buttondown.email/cryptography-dispatches/archive/cryptography-dispatches-registries-considered/

So, I venture out, reading about other things the author has done, just one of which is ...

https://filippo.io/behindthesofa/

... including a specific piece of software ...

https://github.com/google/trillian

... written in a specific language, Go.

That got me to thinking about various 21st century languages and their implementations. 

Because I'm particularly interested in things like diverse double compilation, diversity and decentralization of computing infrastructure either through fully independent self-hosting toolchains or via bootstrapping from one toolchain into another via cross-compilation, I went to see if there is a Go implementation in LLVM.

Turns out there is, but it seems sort of stale. 

https://github.com/go-llvm/llgo

Instead of having to infer this from the age of the weird artifacts that remain ...

http://llvm.org/svn/llvm-project/llgo/trunk/README.TXT

I found explicit confirmation.

https://www.phoronix.com/scan.php?page=news_item&px=LLVM-Drops-LLGO-Golang

This reminded me that LLVM is an example of a project that relicensed, from a custom non-reciprocal BSD-style license to Apache 2.0 with a custom exceptions clause, mostly relieving downstream of the need to display copyright credits in certain situations.

https://en.wikipedia.org/wiki/LLVM

https://releases.llvm.org/9.0.0/LICENSE.TXT

https://hub.packtpub.com/llvm-will-be-relicensing-under-apache-2-0-start-of-next-year/

In the meantime, though, I'm now thinking of all of these copyleft-shunning projects, which eventually led me into corners of the ongoing discussion I hadn't looked quite so deeply into. For instance, Rob Landley's apostasy about GPL licenses is not unfamiliar (to be fair, he says he didn't leave them, they left him).

https://en.wikipedia.org/wiki/Toybox

https://en.wikipedia.org/wiki/BusyBox

Even so, I hadn't read before much if any of the direct back-and-forth between mjg59 and landley:

https://www.computerworld.com/article/2732025/gpl-enforcement-sparks-community-flames.html

https://lwn.net/Articles/478249/

https://mjg59.dreamwidth.org/10437.html

https://mjg59.dreamwidth.org/10437.html?thread=298437#cmt298437

https://mjg59.dreamwidth.org/10437.html?thread=301509#cmt301509

In the course of one of those exchanges, Landley refers to a series of five articles he wrote for Motley Fool. He provided a link, but it's dead.

Turns out they're in the Wayback Machine, though it looks like the Fool changed their web design midway through the week in which they published Landley's articles:

https://web.archive.org/web/20160802163751/https://www.fool.com/portfolios/rulemaker/2000/rulemaker000501.htm

https://web.archive.org/web/20160802153844/https://www.fool.com/portfolios/rulemaker/2000/rulemaker000502.htm/

https://web.archive.org/web/20000815090432/https://www.fool.com/portfolios/rulemaker/2000/rulemaker000503.htm/

https://web.archive.org/web/20000815090426/https://www.fool.com/portfolios/rulemaker/2000/rulemaker000504.htm/

https://web.archive.org/web/20000815090421/https://www.fool.com/portfolios/rulemaker/2000/rulemaker000505.htm/

(It took some digging and trial-and-error to find these. I ended up just inferring that they were in sequentially-named files.
To redeem the effort to dig these up, and not to have to repeat it, is one of the things that motivated me to write this up.)

Contemporaneous with this, Bradley Kuhn wrote something of a coda for a series of talks he had been giving about seeking copyleft compliance:

https://sfconservancy.org/blog/2012/feb/01/gpl-enforcement/

which recontextualizes some of the other discussion about the cross-project reach of compliance efforts and how they're funded, which is plausibly less sinister when Bradley describes his own work than when Landley describes it.

I had completely missed the MEPIS story.

https://lwn.net/Articles/193852/

which reminds me a lot of the concerns I've expressed about Linux Mint to its fans in our local users group.

I knew Bruce Perens had a role in this discussion and its not surprising to see him called out, or to see him respond.

I was not much aware though of "Tim's" participation in this until seeing reference to him in Landley's comments (in response to earlier mentions) so that landed me at:

https://elinux.org/Tim_Bird%27s_Presentations

Trying to find some links to use as a base for non-reciprocal licensing more generally turned into an exercise in further
divergence.

My search first brought me to:

https://en.wikipedia.org/wiki/Reciprocal_Public_License

and then to the complex of links starting at

https://commonstransition.org/commons-based-reciprocity-licenses/

and running through

https://commonstransition.org/a-commons-transition-plan/

https://wiki.p2pfoundation.net/Peer_Production_License

https://wiki.p2pfoundation.net/CopyFair_License

https://wiki.p2pfoundation.net/Copyfarleft

Occaionally I'm happy to find Wikipedia has gotten something extensive and possibly useful to cover various facets of these discussions, in this case:

https://en.wikipedia.org/wiki/License_compatibility

https://en.wikipedia.org/wiki/License_proliferation

https://en.wikipedia.org/wiki/Source-available_software#Commons_Clause


Other miscellany from this process either in the TIL category or that I
can't be fussed to more tightly incorporate into the story:

https://en.wikipedia.org/wiki/Source-available_software#SugarCRM_Public_License

https://hub.packtpub.com/is-the-commons-clause-a-threat-to-open-source/

https://spdx.org/licenses/LLVM-exception.html

https://en.wikipedia.org/wiki/Stet_(software)

https://yarchive.net/comp/linux/gpl.html

https://mjg59.dreamwidth.org/9387.html

http://landley.net/toybox/status.html

https://web.archive.org/web/20060718110636/http://www.mepis.org/

https://lwn.net/Articles/474198/

https://fair.io/

https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licences

https://www.youtube.com/watch?v=SGmtP5Lg_t0&t=15m10s

Overall, I opened, bookmarked, and then closed 67 tabs on this *after* stepping off the path of the cryptography-related links.

