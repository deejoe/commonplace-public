
# GPL Version differences

## Differences around the 'or later' language within the license files

I took the text from the corresponding sections of the GPL-2.0 and of the GPL-3.0 regarding later versions and then calculated a word diff.

```
$ wdiff gpl-2.0-version-comparison.md gpl-3.0-version-comparison.md > ~/commonplace/public/gpl-versions-across-versions.md
```


```
[-9.-]{+14. Revised Versions of this License.+}

The Free Software Foundation may publish revised and/or new versions of the {+GNU+} General Public License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns.

Each version is given a distinguishing version number. If the Program specifies {+that+} a {+certain numbered+} version [-number-] of [-this-] {+the GNU General Public+} License [-which-] {+“or any later version”+} applies to [-it and "any later version",-] {+it,+} you have the option of following the terms and conditions either of that {+numbered+} version or of any later version published by the Free Software Foundation. If the Program does not specify a version number of [-this-] {+the GNU General Public+} License, you may choose any version ever published by the Free Software Foundation.

{+If the Program specifies that a proxy can decide which future versions of the GNU General Public License can be used, that proxy's public statement of acceptance of a version permanently authorizes you to choose that version for the Program.

Later license versions may give you additional or different permissions. However, no additional obligations are imposed on any author or copyright holder as a result of your choosing to follow a later version.+}
```
There is probably commentary somewhere about why the wording changed from version 2 to version 3. 


## Differences in applicability headers or other meta-statements outside of license files

However, the biggest difference with regard to the *or later* type of language has to do with its presence or absence in licensing statements like those shown at the bottom of each of these four examples from the [SPDX list of licenses][10]

[10]: https://spdx.org/licenses/

One can see all four combinations of GPL 2 versus 3 and -only versus -or-later

[GPL-2.0-only][20] 

[20]: https://spdx.org/licenses/GPL-2.0-only.html

[GPL-2.0-or-later][30] 

[30]: https://spdx.org/licenses/GPL-2.0-or-later.html

[GPL-3.0-only][40] 

[40]: https://spdx.org/licenses/GPL-3.0-only.html

[GPL-3.0-or-later][50] 

[50]: https://spdx.org/licenses/GPL-3.0-or-later.html

I think it's important to distinguish amongst uses of the word **license** for distinct, but related, purposes. First, there is the concept of *license* roughly synonymous to *permission* which is to say, what permission does downstream have from upstream? Are you allowed to do a certain thing, and what evidence is there? In this case, we use the word *license* as a concept, an abstraction. 

This leads us to another concept of *license* by which we mean a specific document with specific text, that is to say perhaps more succinctly, a license *file*. We look at a license file to try to figure out what we have explicit permission to do in the given context.

This is important because we often talk about the specific text of license files. But the state of using something *under* a license, or, as we often put it, the state of *having* license to do a certain thing, is distinct from any given text. The permission comes from statements that tie the text of a license to the work that it is meant to cover. 

If I give you a license text without any context, how do you know what it covers, or in fact that it applies to *anything*? 

This is why we have the practice of putting license headers in every file, or at the top level in the directory structure of a given project: We try to keep the license statements prominent and close to the works which are covered to try to make it obvious and unambiguous that they apply to those works.

The Linux kernel, as is often the case, provides prominent examples of these principles being applied. The Linux kernel is (in)famous for rejecting "or later" language, and it's been enough of an issue that Linus Torvalds has attached a Linux-specific statement to kernel code, the [Linux syscall note][60]

[60]: https://spdx.org/licenses/Linux-syscall-note.html

which we can see in, for example, the `/usr/include/linux/tty.h` header file:

```
/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */
```

